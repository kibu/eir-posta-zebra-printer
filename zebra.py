import socket
import config
import error_codes
import warning_codes


def print_zpl(data, debug=False):
    printer_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    printer_socket.connect((config.host, config.port))  # connecting to host
    printer_socket.send(bytes(data, encoding='utf8'))  # using bytes
    errors = get_errors(printer_socket, debug)
    printer_socket.close()  # closing connection
    return errors


def get_errors(printer_socket=None, debug=False):
    status = __get_status(printer_socket, debug)
    return __error_parser(status, debug)


def __get_status(printer_socket=None, debug=False):

    should_close = False

    if printer_socket is None:
        printer_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        printer_socket.connect((config.host, config.port))  # connecting to host
        should_close = True

    printer_socket.send(b"~HQES")  # using bytes
    data = printer_socket.recv(1024)
    status_message = data.decode('utf8')

    if should_close:
        printer_socket.close()

    if debug:
        print(status_message)

    return status_message


def __error_parser(status_message, debug=False):

    errors = {
        'errors': [],
        'warnings': []
    }

    # error
    if status_message[70]:
        error_nibbles = status_message[81:89]

        for nibble in range(5,0,-1):
            if error_nibbles[8 - nibble] != '0':
                key = str(nibble) + error_nibbles[8 - nibble]

                try:
                    errors['errors'].append(error_codes.codes[key])

                except KeyError:
                    print("Unknown error code " + key)

    # warning
    if status_message[116]:
        warning_nibbles = status_message[127:135]

        for nibble in range(3, 0, -1):
            if warning_nibbles[8 - nibble] != '0':
                key = str(nibble) + warning_nibbles[8 - nibble]

                try:
                    errors['warnings'].append(warning_codes.codes[key])

                except KeyError:
                    print("Unknown warning code " + key)

    if debug:
        print(errors)

    return errors
