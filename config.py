# zebra
host = "192.168.10.64"
port = 9100

# label
sticker_width = 100
sticker_height = 50
char_height = 12
char_width = 11
gap = 0
padding_left = 3
padding_top = 6
justify = 'C'

uvk_sticker_width = sticker_width/2+2

# smaller label
# sticker_width = 60
# sticker_height = 35
# char_height = 9
# char_width = 8
# gap = 0
# padding_left = 1
# padding_top = 1
# justify = 'C'
