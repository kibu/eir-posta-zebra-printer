import os
from PIL import Image
import datetime
import zpl
import config


def generatezpl(id, name, date_arrival, date_expiry, teteles, tag_id=None, debug=False):

    l = zpl.Label(config.sticker_height, config.sticker_width, dpmm=8)

    # azonosito
    height = config.padding_top
    l.origin(config.padding_left, height)
    l.write_text(id, char_height=config.char_height, char_width=config.char_width,
                 line_width=config.sticker_width, justification=config.justify)
    l.endorigin()

    # nev
    height += config.char_height + config.gap
    l.origin(config.padding_left, height)
    l.write_text(name, char_height=config.char_height, char_width=config.char_width,
                 line_width=config.sticker_width, justification=config.justify)
    l.endorigin()

    # erkezes
    height += config.char_height + config.gap
    l.origin(config.padding_left, height)
    l.write_text(date_arrival, char_height=config.char_height, char_width=config.char_width,
                 line_width=config.sticker_width, justification=config.justify)
    l.endorigin()

    if teteles:
        l.origin(0, height)
        l.write_text("V", char_height=config.char_height*2, char_width=config.char_width*2,
                     line_width=config.sticker_width-10, justification="R")
        l.endorigin()

    # lejarat
    height += config.char_height + config.gap
    l.origin(config.padding_left, height)
    l.write_text(date_expiry, char_height=config.char_height, char_width=config.char_width,
                 line_width=config.sticker_width, justification=config.justify)
    l.endorigin()

    if tag_id is not None:
        l.origin(config.padding_left, height+3)
        l.write_text('TAG:' + tag_id, char_height=config.char_height-7, char_width=config.char_width-7,
                     line_width=config.sticker_width, justification="L")
        l.endorigin()

    zpl_data = "^XA^CI28" + l.dumpZPL()[3:]

    if debug:
        print(zpl_data)
        l.preview()

    return zpl_data


def generate_uvk_zpl(uvk_cause_id, postal_code, debug=False):
    # 8 - csomagnyomozó
    # 9 - egyéb

    # új irányítószám, ha a csomagnyomozóba megy
    if uvk_cause_id == 8: postal_code = 1811

    l = zpl.Label(config.sticker_height, config.sticker_width, dpmm=8)

    # layout
    height = 2
    l.origin(config.padding_left, height)
    image_height = l.write_graphic(Image.open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                                           'imgs/UVK_MATRI.png')), width=config.uvk_sticker_width)
    l.endorigin()

    # indok
    text = 'X'
    if uvk_cause_id > 7:
        x = config.padding_left + 3
        y = 11+4*6

        l.origin(x, y)
        if uvk_cause_id == 8: text = 'őrzés csomagnyomozó'
        elif uvk_cause_id == 9: text = 'egyéb:'

    else:
        column = 3 if uvk_cause_id < 4 else 28
        row = uvk_cause_id if uvk_cause_id < 4 else uvk_cause_id - 4
        x = config.padding_left+column
        y = 11+row*6

    l.origin(x, y)
    l.write_text(text, char_height=4, char_width=3, line_width=config.sticker_width, justification='L')

    l.endorigin()

    # iranyitoszam
    postal_code = str(postal_code)
    p = ''.join(s+' ' for s in postal_code)
    l.origin(config.uvk_sticker_width/2+9, 3.5)
    l.write_text(p, char_height=5, char_width=6,
                 line_width=config.sticker_width, justification='L')
    l.endorigin()

    # visszaküldés dátuma
    today = datetime.date.today()
    uvk_date = '{}  {}  {}'.format(str(today.year)[2:],
                               today.month if today.month > 9 else '0{}'.format(today.month),
                               today.month if today.day > 9 else '0{}'.format(today.day))
    l.origin(28, 41.5)
    l.write_text(uvk_date, char_height=5, char_width=6,
                 line_width=config.sticker_width, justification='L')
    l.endorigin()

    zpl_data = "^XA^CI28" + l.dumpZPL()[3:]

    if debug:
        print(zpl_data)
        l.preview()

    return zpl_data
